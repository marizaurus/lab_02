﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.IO;
using System.Reflection;

namespace Lab_02
{
    public class Threat
    {
        public string ThreatID { get; set; }
        public string ThreatName { get; set; }
        public string ThreatDescr { get; set; }
        public string ThreatSource { get; set; }
        public string ThreatObj { get; set; }
        public string BreakConf { get; set; }
        public string BreakIntegr { get; set; }
        public string BreakAvail { get; set; }

        public static int ThreatCount { get; set; }
        public static List<Threat> Threats;

        public Threat() { }
        public Threat(string[] infoArr)
        {
            ThreatID = string.Format("УБИ.{0:d3}", int.Parse(infoArr[0]));
            ThreatName = infoArr[1];
            ThreatDescr = infoArr[2];
            ThreatSource = infoArr[3];
            ThreatObj = infoArr[4];
            BreakConf = infoArr[5];
            BreakIntegr = infoArr[6];
            BreakAvail = infoArr[7];
        }

        public static void GetAllRecords()
        {
            string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string filename = Path.Combine(executableLocation, "thrlist.xlsx");
            Excel.Application ExApp = new Excel.Application();
            Excel.Workbook Wb = ExApp.Workbooks.Open(filename);
            Excel.Worksheet Ws = (Excel.Worksheet)Wb.Sheets[1];
            Excel.Range excelRange = Ws.UsedRange;
            ThreatCount = excelRange.Rows.Count - 2;

            List<Threat> threats = new List<Threat>();
            for (int row = 3; row < ThreatCount + 3; row++)
            {
                string[] strArray = new string[8];
                try
                {
                    for (int col = 1; col <= 8; col++)
                    {
                        strArray[col - 1] = (excelRange.Cells[row, col] as Excel.Range).Value2.ToString();
                    }
                    threats.Add(new Threat(strArray));
                }
                catch
                {
                    continue;
                }                
            }
            Wb.Close();
            ExApp.Quit();
            Threats = threats;
        }

        public static string CompareFiles()
        {
            Excel.Application ExApp = new Excel.Application();
            try
            {
                string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string filename = Path.Combine(executableLocation, "thrlist1.xlsx");
                Excel.Workbook NewWb = ExApp.Workbooks.Open(filename);
                Excel.Worksheet NewWs = (Excel.Worksheet)NewWb.Sheets[1];
                Excel.Range excelRange = NewWs.UsedRange;
                int newThreatCount = excelRange.Rows.Count - 2;

                List<Threat> newThreats = new List<Threat>();
                for (int row = 3; row < newThreatCount + 3; row++)
                {
                    string[] strArray = new string[8];
                    for (int col = 1; col <= 8; col++)
                    {
                        strArray[col - 1] = (excelRange.Cells[row, col] as Excel.Range).Value2.ToString();
                    }
                    newThreats.Add(new Threat(strArray));
                }
                int chThrCounter = 0;
                string changes = "";
                for (int i = 0; i < Threats.Count; i++)
                {
                    if (!ThreatsAreEqual(Threats[i], newThreats[i]) && !ThreatsAreMixed(newThreats[i]))
                    {
                        changes += Threats[i].ThreatID + "\n\n";
                        changes += ChangedFields(Threats[i], newThreats[i]);
                        chThrCounter++;
                    }
                }
                if (newThreatCount > Threats.Count)
                {
                    for(int i = Threats.Count; i < newThreatCount; i++)
                    {
                        if (!ThreatsAreMixed(newThreats[i]))
                        {
                            changes += newThreats[i].ThreatID + "\n\n";
                            changes += ChangedFields(new Threat(), newThreats[i]);
                            chThrCounter++;
                        }
                    }
                }
                changes = string.Format("Статус обновления: Успешно \nВсего изменено записей: {0}\n\n", chThrCounter) + changes;
                Threats = newThreats;
                NewWb.Close();
                ExApp.Quit();
                File.Delete("thrlist.xlsx");
                File.Move("thrlist1.xlsx", "thrlist.xlsx");
                return changes;
            }
            catch(Exception e)
            {
                ExApp.Quit();
                return string.Format("Статус обновления: Ошибка \nПричина ошибки: {0}", e.Message);
            }
        }

        public static bool ThreatsAreEqual(Threat th1, Threat th2)
        {
            return (th1.ThreatID == th2.ThreatID) && 
                (th1.ThreatName == th2.ThreatName) && 
                (th1.ThreatDescr == th2.ThreatDescr) && 
                (th1.ThreatSource == th2.ThreatSource) && 
                (th1.ThreatObj == th2.ThreatObj) && 
                (th1.BreakConf == th2.BreakConf) && 
                (th1.BreakIntegr == th2.BreakIntegr) && 
                (th1.BreakAvail == th2.BreakAvail);
        }
        public static bool ThreatsAreMixed(Threat th)
        {
            bool exists = false;
            int i = 0;
            while (!exists && (i < Threats.Count))
            {
                if (ThreatsAreEqual(th, Threats[i]))
                    exists = true;
                i++;
            }
            return exists;
        }

        public static string ChangedFields(Threat th1, Threat th2)
        {
            string chStr = "";
            if (th1.ThreatName != th2.ThreatName)
                chStr += Info(th1.ThreatName, th2.ThreatName);
            if (th1.ThreatDescr != th2.ThreatDescr)
                chStr += Info(th1.ThreatDescr, th2.ThreatDescr);
            if (th1.ThreatSource != th2.ThreatSource)
                chStr += Info(th1.ThreatSource, th2.ThreatSource);
            if (th1.ThreatObj != th2.ThreatObj)
                chStr += Info(th1.ThreatObj, th2.ThreatObj);
            if (th1.BreakConf != th2.BreakConf)
                chStr += Info(th1.BreakConf, th2.BreakConf);
            if (th1.BreakIntegr != th2.BreakIntegr)
                chStr += Info(th1.BreakIntegr, th2.BreakIntegr);
            if (th1.BreakAvail != th2.BreakAvail)
                chStr += Info(th1.BreakAvail, th2.BreakAvail);
            return chStr;
        }
        private static string Info(string s1, string s2)
        {
            return string.Format("БЫЛО: {0}\nСТАЛО: {1}\n\n", s1, s2);
        }

        public static DataTable GetPage(int currentPage)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Идентификатор УБИ", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Наименование УБИ", Type.GetType("System.String")));
            int n = Math.Min(Threats.Count, (currentPage + 1) * 25);
            int recordsLeft = Math.Min(Threats.Count, (currentPage + 1) * 25);
            for (int row = currentPage * 25; row < n; row++)
            {
                dt.Rows.Add(new object[] { Threats[row].ThreatID, Threats[row].ThreatName });
            }
            return dt;
        }

        public static void SaveFile(string writePath)
        {            
            StreamWriter sw = new StreamWriter(writePath, false, Encoding.Default);
            foreach(Threat threat in Threats)
            {
                sw.WriteLine(threat.ThreatID + " | " +
                    threat.ThreatName + " | " +
                    threat.ThreatDescr + " | " +
                    threat.ThreatSource + " | " +
                    threat.ThreatObj + " | " +
                    threat.BreakConf + " | " +
                    threat.BreakIntegr + " | " +
                    threat.BreakAvail);
            }
            sw.Close();
        }
    }
}
