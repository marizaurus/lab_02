﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lab_02
{
    public partial class ThreatWindow : Window
    {
        public Threat CurrentThreat { get; set; }

        public ThreatWindow(Threat threat)
        {
            CurrentThreat = threat;
            InitializeComponent();
            GetInfo();
        }

        public ThreatWindow(string changes)
        {
            InitializeComponent();
            ShowChanges(changes);
        }

        private void GetInfo()
        {
            textBlock.Text =
                "Идентификатор УБИ: " + CurrentThreat.ThreatID + "\n\n" +
                "Наименование УБИ: " + CurrentThreat.ThreatName + "\n\n" +
                "Описание: " + CurrentThreat.ThreatDescr + "\n\n" +
                "Источник угрозы: " + CurrentThreat.ThreatSource + "\n\n" +
                "Объект воздействия: " + CurrentThreat.ThreatObj + "\n\n" +
                "Нарушение конфиденциальности: " + ((CurrentThreat.BreakConf == "1") ? "да" : "нет") + "\n\n" +
                "Нарушение целостности: " + ((CurrentThreat.BreakIntegr == "1") ? "да" : "нет") + "\n\n" +
                "Нарушение доступности: " + ((CurrentThreat.BreakAvail == "1") ? "да" : "нет");
        }
        private void ShowChanges(string changes)
        {
            textBlock.Text = changes;
        }
    }
}
