﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;

namespace Lab_02
{
    public partial class MainWindow : Window
    {
        public int currentPage = 0;
        public string fileDir = "thrlist.xlsx";

        public MainWindow()
        {
            InitializeComponent();
            if (CheckFile())
            {
                Cursor = Cursors.Wait;
                Threat.GetAllRecords();
                ShortInfo();
            }
            else
            {
                if (MessageBox.Show("Отсутствует необходимый файл. Загрузить?", "Загрузка файла", 
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    Close();
                }
                else
                {
                    DownloadFile(fileDir);
                    Threat.GetAllRecords();
                    ShortInfo();
                }
            }
        }

        private void ShortInfo()
        {
            UpdatePage(0);
            if (Threat.Threats.Count > (Threat.Threats.Count / 25) * 25)
                AddButtons((Threat.Threats.Count) / 25 + 1);
            else
                AddButtons((Threat.Threats.Count) / 25);
        }

        private void AddButtons(int num)
        {
            buttonsGrid.RowDefinitions.Add(new RowDefinition() { });
            for (int i = 0; i < num; i++)
            {
                buttonsGrid.ColumnDefinitions.Add(new ColumnDefinition() { });
                Button btn = new Button
                {
                    Name = "Button" + i,
                    Content = i + 1,
                    Width = 40,
                };
                Thickness margin = btn.Margin;
                margin.Left = 10;
                btn.Margin = margin;

                buttonsGrid.Children.Add(btn);
                Grid.SetRow(btn, 1);
                Grid.SetColumn(btn, i);
                btn.Click += Button_Click;
                btn.Tag = i;
            }
        }

        private void UpdatePage(int currentNum)
        {
            threatsGrid.ItemsSource = Threat.GetPage(currentNum).DefaultView;
            Cursor = Cursors.Arrow;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            currentPage = (int)((Control)sender).Tag;
            UpdatePage(currentPage);
        }

        private void ThreatsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            int threatNum = threatsGrid.SelectedIndex + 25 * currentPage;
            ThreatWindow th = new ThreatWindow(Threat.Threats[threatNum]);
            th.Show();
        }

        private void DownloadFile(string fileDir)
        {
            using (WebClient wc = new WebClient())
            {
                wc.DownloadFileAsync(new Uri("https://bdu.fstec.ru/documents/files/thrlist.xlsx"), fileDir);
            }
        }

        private bool CheckFile()
        {
            return File.Exists(fileDir);
        }
        private void Btn_Refresh(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;
            string newFileDir = "thrlist1.xlsx";
            DownloadFile(newFileDir);
            ThreatWindow th = new ThreatWindow(Threat.CompareFiles());
            UpdatePage(currentPage);
            buttonsGrid.ColumnDefinitions.Clear();
            if (Threat.Threats.Count > (Threat.Threats.Count / 25) * 25)
                AddButtons((Threat.Threats.Count) / 25 + 1);
            else
                AddButtons((Threat.Threats.Count) / 25);
            th.Show();
            Cursor = Cursors.Arrow;
        }
        private void Btn_Save(object sender, RoutedEventArgs e)
        {
            string userName = Environment.UserName;
            string writePath = string.Format("C:\\Users\\{0}\\Documents\\thrlist.txt", userName);
            Threat.SaveFile(writePath);
            MessageBox.Show(string.Format("Файл сохранен в директории: \n{0}", writePath));
        }
    }
}
